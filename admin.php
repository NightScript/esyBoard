<?php
//  AcmlmBoard XD - Administration hub page
//  Access: administrators

include("lib/common.php");

if($loguser['powerlevel'] < 3)
	Kill("You're not an administrator. There is nothing for you here.");

$title = "Administration";

write(
"
	<table class=\"outline\">
	<tr>
	<td class=\"outline cell0 margin faq width50\">
		<ul>
			<li><a href=\"recalc.php\">Recalculate Stats</a></li>
			<li><a href=\"ipbans.php\">Manage IP bans</a></li>
			<li><a href=\"managemods.php\">Manage local moderator assignments</a></li>
			<li><a href=\"editfora.php\">Edit forum list</a></li>
			<li><a href=\"editcats.php\">Edit category list</a></li>
			<li><a href=\"editsettings.php\">Edit General Settings</a></li>
			<li><a href=\"editpora.php\">Edit the PoRA</a></li>
			<li><a href=\"editfaq.php\">Edit the FAQ</a></li>
		</ul>
	</td>
	<td class=\"outline margin cell1 faq width100\">
		<dl>
			<dt>Last viewcount milestone</dt>
			<dd>{0}</dd>
		</dl>
	</td></tr></table>
",	$misc['milestone']);

?>
