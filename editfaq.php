<?php
// esyBoard FAQ editor

require("lib/common.php");

if (!$loguser || $loguser['powerlevel'] < 2)
Kill("You cannot perform this action.");

$faq = getSetting("faqtext");

if (isset($_POST['submit'])) {
Query("UPDATE settings SET value='".$_POST['faqtext']."' WHERE setting='faqtext'");
Redirect("FAQ edited!", "faq.php", "the FAQ");
}

write("<table class=\"outline margin width100\">
<form action=\"editfaq.php\" method=\"post\">
<tr class=\"header0\"><th>Edit the FAQ</th></tr>
<tr>
<td class=\"cell2 center\"><textarea style=\"width:90%;height:576px;\" name=\"faqtext\">{0}</textarea></td>
</tr>
<tr>
<td class=\"cell1\">You can use several custom HTML tags in the FAQ - refer to faq.php's code to know what these are.</td>
</tr>
<tr>
<td class=\"cell0\"><input type=\"submit\" name=\"submit\"></input></td>
</tr>
</form>
</table>
", $faq);