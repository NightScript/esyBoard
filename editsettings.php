<?php
// esyBoard - Settings editor (thanks Nadia-h from the ABXD team for giving me help <3)

require("lib/common.php");

if (!$loguserid || $loguser['powerlevel'] < 3)
Kill("lol no");

$settingsQ = Query("SELECT * from settings");
while($settingsL = Fetch($settingsQ)) {
if ($settingsL['setting'] == "faqtext")
continue;
if ($settingsL['setting'] == "poratitle")
continue;
if ($settingsL['setting'] == "porabox")
continue;
$settingsM .= format("
<tr>
<td class=\"cell2\">{0}</td>
<td class=\"cell1\"><input name=\"{0}\" type=\"text\" value=\"{1}\" /></td>
</tr>
", $settingsL[0], $settingsL[1]);
}

if (isset($_POST['submit'])) {
// welcome to the query hall - clean this up or lolz will happen
Query("UPDATE settings SET value='".$_POST['boardname']."' WHERE setting='boardname'");
Query("UPDATE settings SET value='".$_POST['dateformat']."' WHERE setting='dateformat'");
Query("UPDATE settings SET value='".$_POST['autolockmonths']."' WHERE setting='autolockmonths'");
Query("UPDATE settings SET value='".$_POST['warnmonths']."' WHERE setting='warnmonths'");
Query("UPDATE settings SET value='".$_POST['customtitle']."' WHERE setting='customtitle'");
Query("UPDATE settings SET value='".$_POST['viewcount']."' WHERE setting='viewcount'");
Query("UPDATE settings SET value='".$_POST['theword']."' WHERE setting='theword'");
Query("UPDATE settings SET value='".$_POST['minwords']."' WHERE setting='minwords'");
Query("UPDATE settings SET value='".$_POST['floodint']."' WHERE setting='floodint'");
Query("UPDATE settings SET value='".$_POST['uploadercap']."' WHERE setting='uploadercap'");
Query("UPDATE settings SET value='".justEscape($_POST['profilepreviewtext'])."' WHERE setting='profilepreviewtext'");
Query("UPDATE settings SET value='".$_POST['metadesc']."' WHERE setting='metadesc'");
Query("UPDATE settings SET value='".$_POST['defaulttheme']."' WHERE setting='defaulttheme'");
Query("UPDATE settings SET value='".$_POST['annc']."' WHERE setting='annc'");
Query("UPDATE settings SET value='".$_POST['spatula']."' WHERE setting='spatula'");
Query("UPDATE settings SET value='".$_POST['off']."' WHERE setting='off'");
Redirect("Settings updated!", "admin.php", "the admin panel");
}

write("<table class=\"outline margin width100\"><form action=\"editsettings.php\" method=\"post\">
<tr class=\"header0\"><th colspan=2>Edit Settings</th></tr>
{0}
<tr><td class=\"cell0\" colspan=2><input name=\"submit\" type=\"submit\" /></td></tr>
</form></table>", $settingsM);
?>