<?php
//  AcmlmBoard XD - Theme list

//This file lists all the possible choices for user themes.

$themes = array(
	"esyboard"=>"esyBoard",
	"esylight"=>"esyBoard Light",
);

$themeBylines = array(
	"esyboard"=>"Design by loliesy",
	"esylight"=>"Design by loliesy",
);

$themeOrder = array(
	"esyboard",
	"esylight",
);

?>
