<?php
//  AcmlmBoard XD support - System feedback

function Debug($s)
{
	write("<strong>Debug</strong>: {0}<br />", $s);
}

//	Not really much different to kill()
function Alert($s, $t="Alert")
{
	write("<table class=\"outline margin\"><tr class=\"header0\"><th><strong>{1}</strong></th></tr><tr><td class=\"errorc cell2\">{0}</td></tr></table>", $s, $t);
}

function Kill($s, $t="Error")
{
	Alert($s, $t);
	exit();
}

function Redirect($s,$t,$n)
{
	write(
"
	<table class=\"outline margin\">
		<tr class=\"header0 errort\"><th>
			<strong>{0}</strong>
		</th></tr><tr>
		<td class=\"errorc cell2\">
			You will now be redirected to <a href=\"{1}\">{2}</a>&hellip;
			<div class=\"pollbarContainer\" style=\"margin: 4px auto; width: 25%; display: none;\">
				<div class=\"pollbar\" id=\"theBar\" style=\"background: silver; width: 1%;\">&nbsp;</div>
			</div>
		</td></tr>
	</table>
	<meta http-equiv=\"REFRESH\" content=\"5;URL={1}\" />
	<script type=\"text/javascript\">
		var barWidth = 1;
		var target = \"{1}\";
		
		function doBar()
		{
			barWidth += 5; //use 2 here for smoother animation
			if (barWidth > 101)
			{
				document.location = target;
			}
			else
			{
				if(barWidth > 100)
					theBar.style['width'] = \"100%\";
				else
					theBar.style['width'] = barWidth + \"%\";
				setTimeout(\"doBar()\", 50); //use 20 here for smoother animation
			}
		}
		
		window.onload = function()
		{
			theBar = document.getElementById(\"theBar\");
			theBar.parentNode.style['display'] = \"block\";
			doBar();
		}
	</script>
",	$s, $t, $n);
	exit();
}

?>
