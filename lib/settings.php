<?php
//Generated and parsed by the Board Settings admin panel.
include("settingssystem.php");
//Settings
$boardname = getSetting("boardname");
$logoalt = getSetting("boardname");
$logotitle = getSetting("boardname");
$dateformat = getSetting("dateformat");
$autoLockMonths = getSetting("autolockmonths");
$warnMonths = getSetting("warnmonths");
$customTitleThreshold = getSetting("customtitle");
$viewcountInterval = getSetting("viewcount");
$overallTidy = 0;
$theWord = getSetting("theword");
$systemUser = 0;
$minWords = getSetting("minwords");
$minSeconds = getSetting("floodint");
$uploaderCap = getSetting("uploadercap");
$uploaderWhitelist = getSetting("uploaderwhitelist");

//Hacks
$hacks['forcetheme'] = -1;
$hacks['themenames'] = 0;
$hacks['postfilter'] = 0;

//Profile Preview Post
$profilePreviewText = getSetting("profilepreviewtext");
//Meta
$metaDescription = getSetting("metadesc");
$metaKeywords = "";
?>
