<?php

// Don't want syndromes? Just keep this array empty!

$aff = "Affected by ";

$syndromes = array
(
	//This is the set from esyBoard
	5 => array("$aff'MIPS' Syndrome", "yellow"),
	10 => array("$aff'Cosmic Eternity' Syndrome", "grey"),
	25 => array("$aff'Fifty Hertz' Syndrome", "green"),
	50 => array("$aff'Progressive Scan Frames' Syndrome", "skyblue"),
	100 => array("$aff'Centurion' Syndrome", "purple"),

);

?>
