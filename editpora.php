<?php
// esyBoard PoRA editor

require("lib/common.php");

if (!$loguser || $loguser['powerlevel'] < 2)
Kill("You cannot perform this action.");

$pora1 = getSetting("poratitle");
$pora2 = getSetting("porabox");

if (isset($_POST['submit'])) {
Query("UPDATE settings SET value='".justEscape($_POST['pora2'])."' WHERE setting='porabox'");
Query("UPDATE settings SET value='".$_POST['pora1']."' WHERE setting='poratitle'");
Redirect("PoRA edited!", "index.php", "the Index");
}

write("<table class=\"outline margin width100\">
<form action=\"editpora.php\" method=\"post\">
<tr class=\"header0\"><th>Edit the PoRA</th></tr>
<tr>
<td class=\"cell2 center\"><input type=\"text\" name=\"pora1\" value=\"{1}\" /></td>
</tr>
<tr>
<td class=\"cell2 center\"><textarea style=\"width:90%;height:576px;\" name=\"pora2\">{0}</textarea></td>
</tr>
<tr>
<td class=\"cell0\"><input type=\"submit\" name=\"submit\"></input></td>
</tr>
</form>
</table>
", $pora2, $pora1);
